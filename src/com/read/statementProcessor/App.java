package com.read.statementProcessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class App {
	public static void main(String[] args) {
		processCSVFileRecords();
		processXMLFileRecords();
	}

	private static void processXMLFileRecords() {
		try {
			File file = new File("records.xml");
			// an instance of factory that gives a document builder
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			// an instance of builder to parse the specified xml file
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file.getAbsoluteFile());
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("record");
			Map<String, String> map = new HashMap<>();

			for (int itr = 0; itr < nodeList.getLength(); itr++) {
				Node node = nodeList.item(itr);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) node;
					String referenceNo = node.getAttributes().getNamedItem("reference").getTextContent();
					String endBalance = eElement.getElementsByTagName("endBalance").item(0).getTextContent();
					if (map.get(referenceNo) == null) {
						if (Float.parseFloat(endBalance) < 0) {
							System.out.println(referenceNo + "----" + "End Balance is negative");
						} else {
							map.put(referenceNo, "N");
						}
					} else {
						if (map.get(referenceNo).equals("N")) {
							System.out.println(referenceNo + "----" + "Duplicate reference number");
							map.put(referenceNo, "Y");
						}
					}
				}
			}
			System.out.println("\n----XML file processed successfully----");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void processCSVFileRecords() {
		// parsing a CSV file into Scanner class constructor
		File file = new File("records.csv");
		Scanner sc;
		try {
			sc = new Scanner(file.getAbsoluteFile());
			sc.nextLine(); // sets the delimiter pattern
			Map<String, String> map = new HashMap<>();
			while (sc.hasNext()) // returns a boolean value
			{
				String[] data = sc.nextLine().split(",");
				if (map.get(data[0]) == null) {
					if (Float.parseFloat(data[5]) < 0) {
						System.out.println(data[0] + "----" + "End Balance is negative");
					} else {
						map.put(data[0], "N");
					}
				} else {
					if (map.get(data[0]).equals("N")) {
						System.out.println(data[0] + "----" + "Duplicate reference number");
						map.put(data[0], "Y");
					}
				}
			}
			System.out.println("----CSV file processed successfully----\n");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
